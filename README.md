# NASM stack
Реализация стека на языке ассемблера NASM (Netwide ASM) для первой лабораторной работы по курсу "Модели нарушения безопасности и вирусология" (осенний семестр 2018/2019 СПбГЭТУ).

Сделано:

* собственная реализация функций POP и PUSH
* проверка перполнения стека с помощью механизма "осведомителя"

студенты группы 4362

Георгица Андрей

Тищенко Виталия

* * *

Stack implementation for the first lab work of сomputer virology course (fall semester of 2018/2019 academic year, Saint-Petersburg ETU)

Result:

* POP and PUSH functions implementation
* "canaries" overflow check

group №4362

Andrey Georgitsa

Tishchenko Vitaliya


